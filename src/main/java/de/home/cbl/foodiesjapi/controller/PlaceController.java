package de.home.cbl.foodiesjapi.controller;

import de.home.cbl.foodiesjapi.Constants;
import de.home.cbl.foodiesjapi.dto.PlaceDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(Constants.PLACES)
public class PlaceController {

    Logger logger = LoggerFactory.getLogger(PlaceController.class);

    private PlaceService placeService;


    public PlaceController(@Autowired final PlaceService placeService) {
        this.placeService = placeService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {
            "page", "size"})
    public ResponseEntity<Page<PlaceDTO>> getPlaces(@RequestParam(value = "page", defaultValue = "0") final int page,
                                                    @RequestParam(value = "size", defaultValue = "0") final int size) {
        final Page<PlaceDTO> resultPage = this.placeService.findPaginated(page, size);
        if (page > resultPage.getTotalPages()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultPage);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity getPlaces(@PathVariable("id") final UUID id) {
        if (!this.placeService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(this.placeService.findPlaceById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<PlaceDTO> createPlace(@RequestBody final PlaceDTO placeDTO) {
        if (this.placeService.existsById(placeDTO.getId())) {
            return ResponseEntity.badRequest().build();
        }

        final PlaceDTO createdPlace = this.placeService.create(placeDTO);
        return ResponseEntity.ok(createdPlace);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PlaceDTO> updatePlace(@PathVariable("id") final UUID id,
                                                @RequestBody final PlaceDTO placeDTO) {
        if (!this.placeService.existsById(id)) {
            return ResponseEntity.notFound().build();
        }

        if (!id.equals(placeDTO.getId()) && this.placeService.existsById(placeDTO.getId())) {
            return ResponseEntity.badRequest().build();
        }

        final PlaceDTO updatedPlaceDTO = this.placeService.update(id, placeDTO);
        return ResponseEntity.ok(updatedPlaceDTO);
    }

    @RequestMapping(value = "/near", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PlaceDTO>> findPlaceNearLocation(@RequestParam("lat") final double lat,
                                                                @RequestParam("lng") final double lng,
                                                                @RequestParam(value = "distanceInKilometers", defaultValue = "0.5") final double distanceInKilometers) {
        final List<PlaceDTO> placeDTOs = this.placeService.findPlaceNearLocation(lat, lng, distanceInKilometers);
        return ResponseEntity.ok(placeDTOs);
    }
}
