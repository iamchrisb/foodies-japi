package de.home.cbl.foodiesjapi.controller;

import de.home.cbl.foodiesjapi.dto.PlaceDTO;
import de.home.cbl.foodiesjapi.model.Place;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlaceConverter {

    private ModelMapper modelMapper;

    public PlaceConverter(@Autowired final ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public PlaceDTO convertToDto(final Place place) {
        return this.modelMapper.map(place, PlaceDTO.class);
    }

    public Place convertToEntity(final PlaceDTO placeDTO) {
        return this.modelMapper.map(placeDTO, Place.class);
    }

    public List<Place> convertDTOs(final List<PlaceDTO> dtos) {
        return dtos.stream().map(p -> this.convertToEntity(p)).collect(Collectors.toList());
    }

    public List<PlaceDTO> convertPlaces(final List<Place> places) {
        return places.stream().map(p -> this.convertToDto(p)).collect(Collectors.toList());
    }
}
