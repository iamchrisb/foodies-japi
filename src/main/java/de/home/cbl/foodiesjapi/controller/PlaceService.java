package de.home.cbl.foodiesjapi.controller;

import de.home.cbl.foodiesjapi.dto.PlaceDTO;
import de.home.cbl.foodiesjapi.model.Place;
import de.home.cbl.foodiesjapi.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class PlaceService {

    private PlaceRepository placeRepository;
    private PlaceConverter placeConverter;

    public PlaceService(@Autowired final PlaceRepository placeRepository, @Autowired final PlaceConverter placeConverter) {
        this.placeRepository = placeRepository;
        this.placeConverter = placeConverter;
    }

    public Page<PlaceDTO> findPaginated(final int page, final int size) {
        final List<Place> allPlaces = this.placeRepository.findAll();

        final List<PlaceDTO> placeDTOS = this.placeConverter.convertPlaces(allPlaces);
        final Page<PlaceDTO> pageContent = new PageImpl<>(placeDTOS);
        return pageContent;
    }

    public PlaceDTO findPlaceById(final UUID id) {
        final Place place = this.placeRepository.findById(id).get();
        return this.placeConverter.convertToDto(place);
    }

    public boolean existsById(final UUID id) {
        return this.placeRepository.existsById(id);
    }

    public PlaceDTO create(final PlaceDTO placeDTO) {
        final Place place = this.placeConverter.convertToEntity(placeDTO);
        final Place createdPlace = this.placeRepository.save(place);
        return this.placeConverter.convertToDto(createdPlace);
    }

    public PlaceDTO update(final UUID id, final PlaceDTO placeDTO) {
        // mongodb can not update the _id field so the old entry has to be removed
        final Place place = this.placeConverter.convertToEntity(placeDTO);
        final Place updatedPlace = this.placeRepository.save(place);

        if (!id.equals(placeDTO.getId())) {
            // in case we are not updating the same entry
            this.placeRepository.deleteById(id);
        }

        return this.placeConverter.convertToDto(updatedPlace);
    }

    public List<PlaceDTO> findPlaceNearLocation(final double lat, final double lng, final double distanceInKilometers) {
        final List<Place> places = this.placeRepository
                .findByPositionNear(new Point(lat, lng),
                        new Distance(distanceInKilometers, Metrics.KILOMETERS));
        return this.placeConverter.convertPlaces(places);
    }
}
