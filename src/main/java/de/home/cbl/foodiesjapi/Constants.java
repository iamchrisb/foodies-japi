package de.home.cbl.foodiesjapi;

public class Constants {

  public static final String API = "/api";
  public static final String API_VERSION = API + "/v1";
  public static final String PLACES = API_VERSION + "/places";
}
