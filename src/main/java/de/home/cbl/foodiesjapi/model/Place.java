package de.home.cbl.foodiesjapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Place {

    @Id
    private UUID id;

    @Indexed
    private String title;

    private String description;

    private LocalDate published;

    private LocalDateTime lastUpdated;

    private List<String> pictures;

    @GeoSpatialIndexed
    private double[] position;

}
