package de.home.cbl.foodiesjapi.repository;

import de.home.cbl.foodiesjapi.model.Place;
import java.util.List;
import java.util.UUID;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlaceRepository extends MongoRepository<Place, UUID> {

  List<Place> findByPositionNear(Point p, Distance d);

}
