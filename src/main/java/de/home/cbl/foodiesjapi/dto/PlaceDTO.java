package de.home.cbl.foodiesjapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlaceDTO {
    private UUID id;

    private String title;

    private String description;

    private LocalDate published;

    private LocalDateTime lastUpdated;

    private List<String> pictures;

    private double[] position;
}
